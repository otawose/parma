'''
    File:   eqs_genMPI+IO.py
    Author: otawose@nevada.unr.edu
    Date:   3/29/2018
    Desc:   Generate all possible equations given a N*N two-dimensional circuit grid, based on Kirchhoff Circuit Law using MPI (I/O inclusive)
'''

from mpi4py import MPI
import sys
import datetime
import time 
import csv

# Convert a general equation into ij-valued:
def fill_index(i, j, k, m, n, eq):
    res = eq

    # figure out how to map k (m) into [1..n-1] for Ua's and Ub's
    if k != 0:
        k_map_val = k
        if k > j:
            k_map_val = k - 1
        res = res.replace('Ua_ijk', 'Ua['+str(i)+']['+str(j)+']['+str(k_map_val)+']')
    if m != 0:
        m_map_val = m
        if m > i:
            m_map_val = m - 1
        res = res.replace('Ub_ijm', 'Ub['+str(i)+']['+str(j)+']['+str(m_map_val)+']')

    #for U's and R's
    res = res.replace('_i', '['+str(i)+']')
    res = res.replace('j', '['+str(j)+']')
    res = res.replace('k', '['+str(k)+']')
    res = res.replace('_m', '['+str(m)+']')
    
    return res 

# Generate the equations between the i-th row and the j-th column
# Each i-j combination yields 2N equations
def eqs_gen_ij(i, j,n):
    # Enforce I = Sigma(i) at the starting point
    eq_1 = 'U_ij / Z_ij - U_ij / R_ij'
    for k in range(1, n+1):
        if k != j:
            eq_1 += ' - (U_ij - Ua_ijk) / R_ik' 
            eq_1 = fill_index(i, j, k, 0, n, eq_1)

    # Enforce I = Sigma(i) at the end point
    eq_2 = 'U_ij / Z_ij - U_ij / R_ij'
    for m in range(1, n+1):
        if m != i:
            eq_2 += ' - Ub_ijm / R_mj' 
            eq_2 = fill_index(i, j, 0, m, n, eq_2)

    res = [eq_1, eq_2]

    # List all the other 2(n-2) equations for intermediate joints 
    # Left (n-1) eqs:
    for k in range(1, n+1):
        if k != j: 
            eq = '(U_ij - Ua_ijk) / R_ik'        
            for m in range(1, n+1):
                if m != i:
                    eq += ' - (Ua_ijk - Ub_ijm) / R_mk'
                    eq = fill_index(i, j, k, m, n, eq)
            res += [eq]
    # Right (n-1) eqs:
    for m in range(1, n+1):
        if m != i: 
            eq = 'Ub_ijm / R_mj'        
            for k in range(1, n+1):
                if k != j:
                    eq += ' - (Ua_ijk - Ub_ijm) / R_mk'
                    eq = fill_index(i, j, k, m, n, eq)
            res += [eq]

    return res

def main(n):
    array_rank = n
    comm = MPI.COMM_WORLD
    size = MPI.COMM_WORLD.Get_size()
    rank = MPI.COMM_WORLD.Get_rank()
    comm.Barrier()   ### start stopwatch
    t_start = MPI.Wtime()
   
    eqs_even_rank = []
    eqs_odd_rank = []
    if rank % 2 == 0:
        # distributing iterations among processors using cyclic distribution i.e round robin fashion
        for l in range(0+rank, array_rank*array_rank, size):
            i = (l // n) + 1
            j = (l - n * (l // n)) + 1     # or j = (l % n) + 1
            eqs_even_rank += eqs_gen_ij(i, j,n)
    else :
        #  distributing iterations among processors using cyclic distribution i.e round robin fashion
        for l in range(0+rank, array_rank*array_rank, size):
            i = (l // n) + 1
            j = (l - n * (l // n)) + 1
            eqs_odd_rank += eqs_gen_ij(i, j,n)

    eqs_total_even = comm.reduce( eqs_even_rank,op=MPI.SUM , root=0   )
    eqs_total_odd = comm.reduce( eqs_odd_rank,op=MPI.SUM , root=0   )

    if rank == 0:
        eqs_total = eqs_total_even + eqs_total_odd
        name = "Eqs_total.txt"
        with open(name, "w") as f:
            f.write('\n'.join(eqs_total))

    comm.Barrier()
    t_end = MPI.Wtime()  ##stop stopwatch

    start_min = comm.reduce( t_start,op=MPI.MIN , root=0   )
    end_max = comm.reduce( t_end,op=MPI.MAX , root=0   )
    
    if rank == 0:
        t_diff = end_max - start_min
        with open('PresultMPI_+IO_chunk.csv', 'a') as newFile:
            newFileWriter = csv.writer(newFile)
            newFileWriter.writerow([n, t_diff,len(eqs_total)])  

# For test only
if __name__ == '__main__':
    n = int(sys.argv[1])   
    main(n)