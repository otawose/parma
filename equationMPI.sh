#! /bin/bash

# File: equationMPI.sh
# Author: otawose@nevada.unr.edu
# Date: 4/1/2019
# Desc: bash script to run MPI Implementation on the on-premises system 
#                      How to run scipt
# chmod +x equationMPI.sh
# ./equationMPI.sh 

for ((i=10;i<=100;i+=10))   ### outer loop for array size up to 100 ###
do
    mpirun -np 48 -f machinefile python eqs_genMPI.py $i  # change to eqs_genMPI_IO.py for I/O time inclusive
done
